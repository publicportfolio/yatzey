var highScores = [];
var getScores = function(){

    $(".firstSixDices").each(function(i){
        i++;
        if (!$(this).prop("disabled")){
            $(this).val(valueSpecificFace(i));
        }
    });
    if (!$("#onePair").prop("disabled")){
        $("#onePair").val(valueOnePair());
    }
    if (!$("#twoPair").prop("disabled")){
        $("#twoPair").val(valueTwoPair());
    }
    if (!$("#threeSame").prop("disabled")){
        $("#threeSame").val(valueThree());
    }
    if (!$("#fourSame").prop("disabled")){
        $("#fourSame").val(valueFour());
    }
    if (!$("#fullHouse").prop("disabled")){
        $("#fullHouse").val(valueFullHouse());
    }
    if (!$("#smallStraight").prop("disabled")){
        $("#smallStraight").val(valueSmallStraight());
    }
    if (!$("#largeStraight").prop("disabled")){
        $("#largeStraight").val(valueLargeStraight());
    }
    if (!$("#chance").prop("disabled")){
        $("#chance").val(valueChance());
    }
    if (!$("#yatze").prop("disabled")){
        $("#yatze").val(valueYatzy());
    }

};

var getSum = function(val){
    var sum = +$("#sum").val();

    var result = sum + val;
    $("#sum").val(result);

    if($("#sum").val() >= 63 && $("#bonus").val() != 50){
        $("#bonus").val(50);
        getTotal(50);
    }
};

var compare = function(a, b){
    return a.score - b.score;
}

function resetGame(){
    $("#throwDice").prop("value","Spil igen");
    $("#highScore").empty();
    var name = prompt("Tillykke du fik " + $("#total").val() + " point. Skriv dit navn:");
    player = {};
    player["name"] = name;
    player["score"] = $("#total").val();
    highScores.push(player);
    highScores.sort(compare);
    for(var i = 0; i < highScores.length; i++){
        $("#highScore").prepend("<tr><td>" + highScores[i].name + " </td><td>" + highScores[i].score + " point</td></tr>");
    }
    $("#highScore").prepend("<tr><th>Highscores</th></tr>");
}

function resetAllFields(){
    $("input[type=text]").val("0");
    $("input[type=text]").prop("disabled",false);
    $("#throwDice").prop("value", "KAST");
}

var getTotal = function(val){
    var total = +$("#total").val();

    var result = total + val;

    $("#total").val(result);

    var helper = 0;
    $("#scoreTable input").each(function(){
        if($(this).prop("disabled")){
            helper++;
        }
    });
    if(helper == 15){
        resetGame();
    }

    //Locking all input fields
    $("#scoreTable").find("input").css("pointer-events","none").css("cursor","no-drop");

    //Resetting throwcount
    resetThrowCount();

    //Enable throw button
    $("#throwDice").prop("disabled", false);

    //Disable checkbox
    $(".diceThrowHeader").find("[type=checkbox]").prop('disabled', true);

    clearResults();
};

function clearResults(){
    $(".diceThrowHeader").find("[type=checkbox]").prop('checked', false);

    $("#resultOne").find(".diceValue").removeClass("marked");
    $("#resultTwo").find(".diceValue").removeClass("marked");
    $("#resultThree").find(".diceValue").removeClass("marked");
    $("#resultFour").find(".diceValue").removeClass("marked");
    $("#resultFive").find(".diceValue").removeClass("marked");
}

