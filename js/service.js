$(document).ready(function () {

    /* Lock input fields */
    $("[type=text]").prop("readonly", true);

    /* After every click, the focus is on the button */
    $("body").on("click", function(){
       $("#throwDice").focus();
    });

    /* Lets you lock the dice by clicking the image */
    $("#resultOne").find(".diceValue").on('click', function(){
        $("#holdOne").click();
        if($("#holdOne").is(":checked")) {
            $(this).addClass("marked");
        }else{
            $(this).removeClass("marked");
        }
    });
    $("#resultTwo").find(".diceValue").on('click', function(){
        $("#holdTwo").click();
        if($("#holdTwo").is(":checked")) {
            $(this).addClass("marked");
        }else{
            $(this).removeClass("marked");
        }
    });
    $("#resultThree").find(".diceValue").on('click', function(){
        $("#holdThree").click();
        if($("#holdThree").is(":checked")) {
            $(this).addClass("marked");
        }else{
            $(this).removeClass("marked");
        }
    });
    $("#resultFour").find(".diceValue").on('click', function(){
        $("#holdFour").click();
        if($("#holdFour").is(":checked")) {
            $(this).addClass("marked");
        }else{
            $(this).removeClass("marked");
        }
    });
    $("#resultFive").find(".diceValue").on('click', function(){
        $("#holdFive").click();
        if($("#holdFive").is(":checked")) {
            $(this).addClass("marked");
        }else{
            $(this).removeClass("marked");
        }
    });
    /* Throw dice */
    $("#throwDice").on("mouseup", function(){

    if ($(this).attr("value") == "Spil igen"){
        resetAllFields();
    }
        throwDiceTurn()
    });

    $("#throwDice").on("keypress", function(e){
        if(e.which == 13){
            if ($(this).attr("value") == "Spil igen"){
                resetAllFields();
            }
            throwDiceTurn()

        }
    });
    function throwDiceTurn() {

        $("#scoreTable").find("input").css("pointer-events","auto").css("cursor","pointer");
        $(".diceThrowHeader").find("[type=checkbox]").prop('disabled', false);

        var holding = [$("#holdOne").is(":checked"), $("#holdTwo").is(":checked"), $("#holdThree").is(":checked"), $("#holdFour").is(":checked"), $("#holdFive").is(":checked")];

        throwDice(holding);

        var result = getValues();

        $("#resultOne").find(".diceValue").css("background-image", "url('img/dices/" + result[0] + ".png')");
        $("#resultTwo").find(".diceValue").css("background-image", "url('img/dices/" + result[1] + ".png')");
        $("#resultThree").find(".diceValue").css("background-image", "url('img/dices/" + result[2] + ".png')");
        $("#resultFour").find(".diceValue").css("background-image", "url('img/dices/" + result[3] + ".png')");
        $("#resultFive").find(".diceValue").css("background-image", "url('img/dices/" + result[4] + ".png')");

        if (getThrowCount() === 3) {
            $("#throwDice").prop("disabled", true);
        }

        $("#throwsLeft").find("b").text(3-throwCount);

        getScores();
    }

    /* allows you to pick a score field */
    $("#scoreTable input").on("click", function () {
        if (!$(this).hasClass("notClickable")) {
            $(this).prop("disabled", true);
            getTotal(+$(this).val());
        }

        if ($(this).hasClass("firstSixDices")) {
            getSum(+$(this).val());
        }

        $("#throwsLeft").find("b").text(3-throwCount);
    });

});

/* Resetting the game */
function resetGameForNewPlayer(){
    resetThrowCount()
}