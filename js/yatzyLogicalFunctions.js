/**
 * Models a game of Yatzy
    /**
     * Face values of the 5 dice. <br/>
     * 1 <= values[i] <= 6.
     */
    var values = [];

    /**
     * Number of times the 5 dice have been thrown. <br/>
     * 0 <= throwCount <= 3.
     */
    var throwCount = 0;

    /**
     * Random number generator.
     */
    var random = Math.random();

    /**
     * Rolls the 5 dice. <br/>
     * Only roll dice that are not hold. <br/>
     * Requires: holds contain 5 boolean values.
     */
    function throwDice(holds) {

    for (var i = 0; i < holds.length; i++) {
    if (!holds[i]) {

    var ran = Math.floor(random * 6 + 1);

    values[i] = ran;
        }
        random = Math.random();
    }

throwCount++;
}

/**
 * Returns the number of times the five dice have been thrown.
 */
function getThrowCount() {
    return throwCount;
}

/**
 * Resets the throw count.
 */
function resetThrowCount() {
    throwCount = 0;
}

/**
 * Get current dice values
 */
function getValues() {
    return values;
}

/**
 * Set the current dice values
 */
function setValues(values) {
    this.values = values;
}

/**
 * Returns all results possible with the current face values. <br/>
 * The order of the results is the same as on the score board.
 */
function getPossibleResults() {
    var results= [];
    for (var i = 0; i <= 5; i++) {
        results[i] = this.valueSpecificFace(i + 1);
    }
    results[6] = this.valueOnePair();
    results[7] = this.valueTwoPair();
    results[8] = this.valueThree();
    results[9] = this.valueFour();
    results[10] = this.valueFullHouse();
    results[11] = this.valueSmallStraight();
    results[12] = this.valueLargeStraight();
    results[13] = this.valueChance();
    results[14] = this.valueYatzy();
    return results;
}

/**
 * Returns an int[7] containing the frequency of face values. <br/>
 * Frequency at index v is the number of dice with the face value v, 1 <= v
 * <= 6. <br/>
 * Index 0 is not used.
 */
function freqFaceValue() {

    var req = [null, 0,0,0,0,0,0];

    for (var i = 0; i < values.length; i++) {

        for (var j = 1; j < 7; j++) {
            if (j == values[i]) {
                req[j]++;
            }
        }
    }
    return req;
}

/**
 * Returns the total value for the dice currently showing the given face
 * value
 *
 * @param face
 *            the face value to return values for
 */
function valueSpecificFace(face) {

    var sum = 0;

    sum = face * freqFaceValue()[face];

    return sum;
}

/**
 * Returns the maximum value for n-of-a-kind for the given n. <br/>
 * For example, valueManyOfAKind(3) returns the maximum value for 3
 * of-a-kind. <br/>
 * Requires: 1 <= faceValue and faceValue <= 6
 *
 * @param n
 *            number of kind
 */
function valueManyOfAKind(n) {

    var valuez = freqFaceValue();
    var sum = 0;

    for (var i = 0; i < valuez.length; i++) {

        if (valuez[i] >= n) {
            sum = (i == 0 ? 1 : i) * n;
        }
    }

    return sum;
}

/**
 * The current value if you try to score the current face values as Yatzy.
 */
function valueYatzy() {

    var yatzy = true;
    var points = 0;

    var toCheck = values[0];

    for (var i = 1; i < values.length; i++) {

        if (values[i] != toCheck) {
            yatzy = false;
        }
    }

    if (yatzy) {
        points = 50;
    }

    return points;
}

/**
 * Returns the current score if used as "chance".
 */
function valueChance() {

    var sum = 0;

    for (var i = 0; i < values.length; i++) {
        sum += values[i];
    }

    return sum;
}

/**
 * Returns the current score for one pair.
 */
function valueOnePair() {

    var numberToCount = 0;
    var count = 0;
    var sum = 0;
    var highest = 0;

    for (var i = 0; i < values.length; i++) {

        numberToCount = values[i];

        for (var j = 0; j < values.length; j++) {

            if (values[j] == values[i] && values[j] == numberToCount) {
                if (j != i && values[j] > highest) {
                    count++;
                    highest = values[j];
                }
            }
        }

        if (count == 1) {
            sum = highest * 2;
            count = 0;
        }
    }

    return sum;
}

/**
 * Returns the current score for two pairs.
 */
function valueTwoPair() {

    var valuez = freqFaceValue();

    var sum = 0;
    var count = 0;

    for (var i = 0; i < valuez.length; i++) {
        if (valuez[i] >= 2) {
            count++;
            sum += (2 * i);
        }
    }

    if (count != 2) {
        sum = 0;
    }

    return sum;
}

/**
 * Returns the current score for three of a kind.
 */
function valueThree() {

    return valueManyOfAKind(3);
}

/**
 * Returns the current score for four of a kind.
 */
function valueFour() {
    return valueManyOfAKind(4);
}

/**
 * Returns the value of a small straight with the current face values.
 */
function valueSmallStraight() {

    var valuez = freqFaceValue();
    var straight = true;
    var sum = 0;

    for (var i = 1; i < valuez.length - 1; i++) {
        if (valuez[6] == 1 || valuez[i] != 1) {
            straight = false;
        }
    }
    if (straight) {
        sum = 15;
    }
    return sum;
}

/**
 * Returns the value of a large straight with the current face values.
 */
function valueLargeStraight() {

    var valuez = freqFaceValue();

    var straight = true;

    var sum = 0;
    for (var i = 2; i < valuez.length; i++) {
        if (valuez[1] == 1 || valuez[i] != 1) {
            straight = false;
        }
    }
    if (straight) {
        sum = 20;
    }
    return sum;
}

/**
 * Returns the value of a full house with the current face values.
 */
function valueFullHouse() {
    var valuez = freqFaceValue();
    var sum3 = 0;
    var sum2 = 0;

    for (var i = 1; i <= 6; i++) {
        if (valuez[i] == 3) {
            sum3 = valuez[i] * i;
        }
    }
    for (var i = 1; i <= 6; i++) {
        if (valuez[i] == 2) {
            sum2 = valuez[i] * i;
        }
    }
    if (sum2 / 2 != sum3 / 3 && sum2 > 0 && sum3 > 0) {
        return sum2 + sum3;
    }
    return 0;
}
